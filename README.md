# TrainingAngular

This project was generated with [Angular CLI](https://github.com/angular/angular-cli) version 10.2.3.

## Development server

Run `ng serve` for a dev server. Navigate to `http://localhost:4200/`. The app will automatically reload if you change any of the source files.

## JSON Server
Chạy file db.json với `npm install -g json-server` để tạo fake server. Navigate to `http://localhost:3000/`.
