import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { BindingComponent } from './binding/binding.component';
import { EditModalComponent } from './edit-modal/edit-modal.component';
import { ListStudentsComponent } from './list-students/list-students.component';

const routes: Routes = [
  {path: '', component: BindingComponent},
  {path: 'binding', component: BindingComponent},
  {path: 'list-students', component: ListStudentsComponent},
  {path: 'edit-modal', component: EditModalComponent},
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule],
})
export class AppRoutingModule { }
