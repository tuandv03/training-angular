import { Component, Input, OnInit, Output, EventEmitter  } from '@angular/core';
import { Student } from '../models/Student';

@Component({
  selector: 'app-edit-modal',
  templateUrl: './edit-modal.component.html',
  styleUrls: ['./edit-modal.component.scss']
})
export class EditModalComponent implements OnInit {


  @Input() student: Student;
  @Output() studentChange: EventEmitter<Student> = new EventEmitter<Student>();

  constructor() {}
  ngOnInit(): void {}

  update(): void {
    this.studentChange.emit(this.student);
  }

}
