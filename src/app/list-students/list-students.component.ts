import { Component, OnInit, TemplateRef } from '@angular/core';
import { BsModalRef, BsModalService } from 'ngx-bootstrap/modal';
import { FormControl } from '@angular/forms';
import { Router } from '@angular/router';
import { ConfigService } from '../config.service';
import { Student } from '../models/Student';

@Component({
  selector: 'app-list-students',
  templateUrl: './list-students.component.html',
  styleUrls: ['./list-students.component.scss']
})
export class ListStudentsComponent implements OnInit {
  search = new FormControl('');
  public students = [];
  public isCheckAll: boolean;
  public checkedStudentList: any;
  public isSelected: boolean;
  public searchText = '';
  modalRef?: BsModalRef;

  public selectedStudent: Student;

  constructor(
    private serviceHttp: ConfigService,
    private router: Router,
    private modalService: BsModalService,
    )
    { }

  ngOnInit(): void {
    this.loadData();
  }

  private loadData(): void {
    this.serviceHttp.getStudents().subscribe((data) => {
      this.students = data;
      this.students.forEach(student => {
        student.isSelected = this.isSelected;
      });
    });
    this.getCheckedStudentList();
  }

  // check box all

  public checkAll(): void {
    console.log(this.students);
    // tslint:disable-next-line:prefer-for-of
    for (let i = 0; i < this.students.length; i++) {
      this.students[i].isSelected = this.isCheckAll;
    }
    this.getCheckedStudentList();
  }

  public checkBox(): void {
    // tslint:disable-next-line:typedef
    this.isCheckAll = this.students.every((item: any) => {
      return item.isSelected === true;
    });

    this.getCheckedStudentList();
  }

  private getCheckedStudentList(): void {
    this.checkedStudentList = [];
    // tslint:disable-next-line:prefer-for-of
    for (let i = 0; i < this.students.length; i++) {
        if (this.students[i].isSelected) {
          this.checkedStudentList.push(this.students[i]);
        }
    }
    // this.checkedStudentList = JSON.stringify(this.checkedStudentList);
  }

  // Tìm kiếm theo tên

  public searchName(e): void {
    this.searchText = e.value;
    console.log(this.searchText);
  }

  openModalWithClass(template: TemplateRef<any>, student): void {

    this.modalRef = this.modalService.show(
      template,
      this.selectedStudent = Object.assign({}, student)
    );
    // console.log(this.selectedStudent);
  }

  update(student): void {
    // console.log(student);
    this.modalRef.hide();
    this.serviceHttp.modifyStudent(student.id, student).subscribe(data => {
      // console.log(data);
      this.loadData();
    });
  }

}
