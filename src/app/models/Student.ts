export interface Student {
  id: string;
  code: string;
  gender: string;
  name: string;
  email?: string;
  phone?: string;
}
