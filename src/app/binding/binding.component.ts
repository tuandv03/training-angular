import { Component, OnInit } from '@angular/core';
import { FormControl, Validators } from '@angular/forms';

@Component({
  selector: 'app-binding',
  templateUrl: './binding.component.html',
  styleUrls: ['./binding.component.scss']
})
export class BindingComponent implements OnInit {

  name = new FormControl('', [Validators.required, Validators.minLength(4)]);
  checked = true;

  constructor() { }

  ngOnInit(): void { }

  public hello(): void {
    alert(`Xin chào ${this.name.value}`);
  }

}
